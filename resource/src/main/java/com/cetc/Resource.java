package com.cetc;

import com.cetc.oauth2.SqliteDataBaseConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({SqliteDataBaseConfig.class})
public class Resource {

    public static void main(String[] args) {
        SpringApplication.run(Resource.class, args);
    }
}
