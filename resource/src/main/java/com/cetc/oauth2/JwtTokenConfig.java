package com.cetc.oauth2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

@Configuration
public class JwtTokenConfig {

    private static KeyPair KEY_PAIR;

    static {
        try {
            KEY_PAIR = KeyPairGenerator.getInstance("RSA").generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
//        accessTokenConverter.setKeyPair(KEY_PAIR);
//        accessTokenConverter.setVerifier(new RsaVerifier(getKeyFromAuthorizationServer()));
        accessTokenConverter.setSigningKey("123");
        return accessTokenConverter;
    }



    @Bean("jwtService")
    protected JwtService jwtService() {
        return new JwtService();
    }

}
