package com.cetc.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;

@Configurable
public class SqliteDataBaseConfig {
	
	@Autowired
    private DataSourceProperties dataSourceProperties;
	
	 @Bean(name="dataSource")
	 public DataSource dataSource() {
	        SQLiteDataSource dataSource = new SQLiteDataSource();
	        dataSource.setUrl(dataSourceProperties.getUrl());
	        return dataSource;
	 }

	 @Bean
	 public JdbcTemplate jdbcTemplate(){
	 	  return new JdbcTemplate(dataSource());
	 }

}
