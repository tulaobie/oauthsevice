package auth.server.inter.pojo;

import java.io.Serializable;

/**
 * @Description  
 * @Author  idea
 * @Date 2019-12-26 
 */

public class OauthClientDetails  implements Serializable {

	private static final long serialVersionUID =  1645018267625526693L;

	private String clientId;

	private String resourceIds;

	private String clientSecret;

	private String scope;

	private String authorizedGrantTypes;

	private String webServerRedirectUri;

	private String authorities;

	private Long accessTokenValidity;

	private Long refreshTokenValidity;

	private String additionalInformation;

	private Boolean autoApprove;

	private String name;

	private Boolean enabled;

	

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getResourceIds() {
		return this.resourceIds;
	}

	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}

	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAuthorizedGrantTypes() {
		return this.authorizedGrantTypes;
	}

	public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}

	public String getWebServerRedirectUri() {
		return this.webServerRedirectUri;
	}

	public void setWebServerRedirectUri(String webServerRedirectUri) {
		this.webServerRedirectUri = webServerRedirectUri;
	}

	public String getAuthorities() {
		return this.authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public Long getAccessTokenValidity() {
		return this.accessTokenValidity;
	}

	public void setAccessTokenValidity(Long accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}

	public Long getRefreshTokenValidity() {
		return this.refreshTokenValidity;
	}

	public void setRefreshTokenValidity(Long refreshTokenValidity) {
		this.refreshTokenValidity = refreshTokenValidity;
	}

	public String getAdditionalInformation() {
		return this.additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public Boolean getAutoApprove() {
		return autoApprove;
	}

	public void setAutoApprove(Boolean autoApprove) {
		this.autoApprove = autoApprove;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "TpApiConfig{" +
				"clientId='" + clientId + '\'' +
				"resourceIds='" + resourceIds + '\'' +
				"clientSecret='" + clientSecret + '\'' +
				"scope='" + scope + '\'' +
				"authorizedGrantTypes='" + authorizedGrantTypes + '\'' +
				"webServerRedirectUri='" + webServerRedirectUri + '\'' +
				"authorities='" + authorities + '\'' +
				"accessTokenValidity='" + accessTokenValidity + '\'' +
				"refreshTokenValidity='" + refreshTokenValidity + '\'' +
				"additionalInformation='" + additionalInformation + '\'' +
				"autoapprove='" + autoApprove + '\'' +
				"name='" + name + '\'' +
				'}';
	}

}
