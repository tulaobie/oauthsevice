package auth.server.inter.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description  
 * @Author  idea
 * @Date 2019-12-26 
 */

public class SysMenu  implements Serializable {

	private static final long serialVersionUID =  1420633030540882129L;

	/**
	 * 菜单ID
	 */
	private Long menuId;

	/**
	 * 菜单名称
	 */
	private String menuName;

	/**
	 * 父菜单ID
	 */
	private Long parentId;

	/**
	 * 显示顺序
	 */
	private Long orderNum;


	/** 路由地址 */
	private String path;

	/** 组件路径 */
	private String component;

	/**
	 * 打开方式（menuItem页签 menuBlank新窗口）
	 */
	private String target;

	/**
	 * 菜单类型（M目录 C菜单 F按钮）
	 */
	private String menuType;

	/**
	 * 菜单状态（0显示 1隐藏）
	 */
	private String visible;

	/**
	 * 权限标识
	 */
	private String perms;

	/**
	 * 菜单图标
	 */
	private String icon;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 系统标识
	 */
	private String systemFlag;


	/**
	 * 数据访问路径(通过请求地址来查询权限),如果一个组件需要多个数据访问路径可以用逗号隔开
	 */
	private String dataComponent;

	public Long getMenuId() {
		return this.menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public String getMenuName() {
		return this.menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getOrderNum() {
		return this.orderNum;
	}

	public void setOrderNum(Long orderNum) {
		this.orderNum = orderNum;
	}

	public String getTarget() {
		return this.target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getMenuType() {
		return this.menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getVisible() {
		return this.visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getPerms() {
		return this.perms;
	}

	public void setPerms(String perms) {
		this.perms = perms;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSystemFlag() {
		return systemFlag;
	}

	public void setSystemFlag(String systemFlag) {
		this.systemFlag = systemFlag;
	}


	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getDataComponent() {
		return dataComponent;
	}

	public void setDataComponent(String dataComponent) {
		this.dataComponent = dataComponent;
	}

	@Override
	public String toString() {
		return "SysMenu{" +
				"menuId=" + menuId +
				", menuName='" + menuName + '\'' +
				", parentId=" + parentId +
				", orderNum=" + orderNum +
				", path='" + path + '\'' +
				", component='" + component + '\'' +
				", target='" + target + '\'' +
				", menuType='" + menuType + '\'' +
				", visible='" + visible + '\'' +
				", perms='" + perms + '\'' +
				", icon='" + icon + '\'' +
				", createBy='" + createBy + '\'' +
				", createTime=" + createTime +
				", updateBy='" + updateBy + '\'' +
				", updateTime=" + updateTime +
				", remark='" + remark + '\'' +
				", systemFlag='" + systemFlag + '\'' +
				'}';
	}
}
