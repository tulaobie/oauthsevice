package auth.server.inter.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description  
 * @Author  idea
 * @Date 2019-12-26 
 */

public class SysUser  implements Serializable {

	private static final long serialVersionUID =  6867944247096711134L;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 部门ID
	 */
	private Long deptId;

	/**
	 * 登录账号
	 */
	private String nickName;

	/**
	 * 用户昵称
	 */
	private String userName;

	/**
	 * 用户类型（00系统用户）
	 */
	private String userType;

	/**
	 * 用户邮箱
	 */
	private String email;

	/**
	 * 手机号码
	 */
	private String phonenumber;

	/**
	 * 用户性别（0男 1女 2未知）
	 */
	private String sex;

	/**
	 * 头像路径
	 */
	private String avatar;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 盐加密
	 */
	private String salt;

	/**
	 * 帐号状态（0正常 1停用）
	 */
	private String status;

	/**
	 * 删除标志（0代表存在 2代表删除）
	 */
	private String delFlag;

	/**
	 * 最后登陆IP
	 */
	private String loginIp;

	/**
	 * 最后登陆时间
	 */
	private Date loginDate;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 备注
	 */
	private String remark;



	/** 角色组 */
	private Long[] roleIds;

	/** 岗位组 */
	private Long[] postIds;


	private String[] roleKeys;


	private String[] postKeys;




	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getDeptId() {
		return this.deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return this.userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhonenumber() {
		return this.phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAvatar() {
		return this.avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getLoginIp() {
		return this.loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public Date getLoginDate() {
		return this.loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}

	public Long[] getPostIds() {
		return postIds;
	}

	public void setPostIds(Long[] postIds) {
		this.postIds = postIds;
	}


	public String[] getRoleKeys() {
		return roleKeys;
	}

	public void setRoleKeys(String[] roleKeys) {
		this.roleKeys = roleKeys;
	}

	public String[] getPostKeys() {
		return postKeys;
	}

	public void setPostKeys(String[] postKeys) {
		this.postKeys = postKeys;
	}

	@Override
	public String toString() {
		return "TpApiConfig{" +
				"userId='" + userId + '\'' +
				"deptId='" + deptId + '\'' +
				"nickName='" + nickName + '\'' +
				"userName='" + userName + '\'' +
				"userType='" + userType + '\'' +
				"email='" + email + '\'' +
				"phonenumber='" + phonenumber + '\'' +
				"sex='" + sex + '\'' +
				"avatar='" + avatar + '\'' +
				"password='" + password + '\'' +
				"salt='" + salt + '\'' +
				"status='" + status + '\'' +
				"delFlag='" + delFlag + '\'' +
				"loginIp='" + loginIp + '\'' +
				"loginDate='" + loginDate + '\'' +
				"createBy='" + createBy + '\'' +
				"createTime='" + createTime + '\'' +
				"updateBy='" + updateBy + '\'' +
				"updateTime='" + updateTime + '\'' +
				"remark='" + remark + '\'' +
				'}';
	}

}
