package auth.server.inter.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientLogin {

     @JsonProperty(value = "access_token")
     private String accessToken;

     @JsonProperty("refresh_token")
     private String refreshToken;

     @JsonProperty("expires_in")
     private Integer expiresIn;

     private String clientId;

     private String clientSecurty;

     private String userName;

     private String userPassword;

     @JsonProperty("token_type")
     private String tokenType;

     
     public ClientLogin(String clientId,String clientSecurty,String userName,String userPassword){
            this.clientId=clientId;
            this.clientSecurty=clientSecurty;
            this.userName=userName;
            this.userPassword=userPassword;
     }

    public ClientLogin() {
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecurty() {
        return clientSecurty;
    }

    public void setClientSecurty(String clientSecurty) {
        this.clientSecurty = clientSecurty;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }
}
