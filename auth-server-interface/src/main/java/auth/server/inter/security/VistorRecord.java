package auth.server.inter.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import tenglang.common.jackson.BeanUtils;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Enumeration;

/**
 * 用户访问情况记录
 */
public class VistorRecord {

    private Long id;

    private String system;

    private String method;

    private String contentType;

    private String fullPath;

    private Boolean isAuth;

    private String userName;

    private Timestamp insertTime;

    private Timestamp updateTime;

    private String errorMessage;

    private Integer responseCode;

    private String responseSource;

    private ObjectNode paramObject;

    public VistorRecord(Long id, String system, String fullPath,String method,String contentType) {
        this.id = id;
        this.system = system;
        this.fullPath = fullPath;
        this.method=method;
        this.contentType=contentType;
        this.insertTime=new Timestamp(System.currentTimeMillis());
        this.updateTime=new Timestamp(System.currentTimeMillis());
    }

    public static VistorRecord  createByRequest(Long id,String bodyContent,HttpServletRequest request){
        Enumeration<String> enumerationHeaderNames=request.getHeaderNames();
        Enumeration<String> enumerationParameterNames=request.getParameterNames();

        VistorRecord vistorRecord=new VistorRecord();

        String contenxt=request.getSession().getServletContext().getContextPath();
        String url=request.getRequestURI();
        url=url.substring(contenxt.length());
        url=url.startsWith("/")?url.substring(1):url;
        String prefix=url.substring(0,url.indexOf("/"));

        vistorRecord.setContentType(request.getContentType());
        vistorRecord.setFullPath(request.getRequestURI());
        vistorRecord.setSystem(prefix);
        vistorRecord.setMethod(request.getMethod());
        vistorRecord.setId(id);
        vistorRecord.setInsertTime(new Timestamp(System.currentTimeMillis()));
        vistorRecord.setUpdateTime(new Timestamp(System.currentTimeMillis()));


        ObjectMapper objectMapper=BeanUtils.getObjectMapper();
        ObjectNode paramObject= objectMapper.createObjectNode();

        paramObject.set("header",objectMapper.createObjectNode());
        paramObject.set("parameter",objectMapper.createObjectNode());

        while(enumerationHeaderNames.hasMoreElements()){
            String headerName=enumerationHeaderNames.nextElement();
            String headerValue=request.getHeader(headerName);
            ((ObjectNode)paramObject.get("header")).put(headerName,headerValue);
        }

        while(enumerationParameterNames.hasMoreElements()){
            String parameterName=enumerationParameterNames.nextElement();
            String[] parameterValue=request.getParameterValues(parameterName);
            ((ObjectNode)paramObject.get("parameter")).put(parameterName,String.join(",",parameterValue));
        }
        paramObject.put("bodyContent",bodyContent);
        vistorRecord.setParamObject(paramObject);
        return vistorRecord;
    }


    public VistorRecord() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public Boolean getAuth() {
        return isAuth;
    }

    public void setAuth(Boolean auth) {
        isAuth = auth;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Timestamp getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Timestamp insertTime) {
        this.insertTime = insertTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public ObjectNode getParamObject() {
        return paramObject;
    }

    public void setParamObject(ObjectNode paramObject) {
        this.paramObject = paramObject;
    }

    public String getResponseSource() {
        return responseSource;
    }

    public void setResponseSource(String responseSource) {
        this.responseSource = responseSource;
    }
}
