package auth.server.inter.service;

import auth.server.inter.pojo.ClientLogin;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "oauthserver",url = "http://localhost:9000")
public interface Oauth2Service {

    @PostMapping(value = "/auth/oauth/token?grant_type=password&scope=users")
    ClientLogin loginByUserAndClient(@RequestParam("client_id") String clientId, @RequestParam("client_secret") String clientSecret,
                                     @RequestParam("username") String username, @RequestParam("password") String password);
    @PostMapping(value = "/auth/oauth/token?grant_type=refresh_token&scope=users")
    ClientLogin refreshByAccessToken(@RequestParam("client_id") String clientId, @RequestParam("client_secret") String clientSecret,@RequestParam("access_token") String accessToken,@RequestHeader(name="Authorization") String authorization);

    @GetMapping(value = "/auth/test/hello")
    String testHello(@RequestParam(name="test") String test);
}
