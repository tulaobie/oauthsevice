package auth.server.inter.service;

import auth.server.inter.pojo.OauthClientDetails;

import java.util.List;

public interface ClientService {

    /**
     * 获取所有的连接端
     * @return
     */
     List<OauthClientDetails> findAllEnableClients();

    /**
     * 通过clientId获取莫一端
     * @return
     */
     OauthClientDetails findClientsByClientId(String clientId);

}
