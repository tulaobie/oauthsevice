package auth.server.inter.service;

import auth.server.inter.pojo.ClientLogin;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "ruoyi",url = "http://localhost:8080")
public interface RuoYiService {

    @PostMapping(value = "/loginByToken",produces = {"application/json"})
    ClientLogin loginByUserAndClient(@RequestBody ClientLogin accessToken);
}
