package auth.server.inter.service;

import auth.server.inter.pojo.AuthorizationToken;
import auth.server.inter.pojo.ClientLogin;
import org.springframework.cloud.openfeign.FeignClient;

public interface AutoLoginService {

    public AuthorizationToken loginByClient(ClientLogin clientLogin);

    public AuthorizationToken refreshTokenByClient(ClientLogin clientLogin);
}
