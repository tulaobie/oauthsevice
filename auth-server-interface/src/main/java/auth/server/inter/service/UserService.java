package auth.server.inter.service;

import auth.server.inter.pojo.SysMenu;
import auth.server.inter.pojo.SysUser;

import java.util.List;

public interface UserService {

     public List<SysUser> findAllEnableUsers();

     public SysUser findUserByUsername(String userName);

     public List<SysMenu> findMenuByUsername(String userName);
}
