package auth.server.inter;


/**
 * 通用常量信息
 * 
 * @author ruoyi
 */
public class AuthConstants
{

    /**
     * 登录用户标识
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    public static final String SECURITY_PREFIX="security:";

    /**
     * 全部的菜单
     */
    public static final String SECURITY_ALL_MENUS_BYURI=SECURITY_PREFIX+"all_menus_uri:";

    /** 所有权限标识 */
    public static final String ALL_PERMISSION = "*:*:*";

    /** 管理员角色权限标识 */
    public static final String SUPER_ADMIN = "admin";

    /**
     * 用户访问记录
     */
    public static final String USER_ACCESS_LOGS="access_logs";

    /**
     * 是否记录访问日志
     */
    public static final String USER_ACCESS_LOGS_ENABLE="access_logs_record";

}
