package tenglang.resource.controller;


import auth.server.inter.pojo.ClientLogin;
import auth.server.inter.service.Oauth2Service;
import auth.server.inter.service.RuoYiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import tenglang.common.web.domain.AjaxResult;

@RestController
@RequestMapping("/auth")
@Api(tags = "身份认证管理")
public class AuthController {

//    @Autowired
//    private TokenService tokenService;
//
//    @Autowired
//    public PermissionService permissionService;

    @Autowired
    private Oauth2Service oauth2Service;

    @Autowired
    private RuoYiService ruoYiService;

    @Value("${token.client-id}")
    private String clientId;

    @Value("${token.client-secret}")
    private String clientSecret;

    @GetMapping(value = "/login")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username",value = "用户名"),
            @ApiImplicitParam(name="password",value = "密码")
    })
    public AjaxResult login(@RequestParam(name = "username") String username, @RequestParam(name = "password") String password){
        ClientLogin clientLogin=oauth2Service.loginByUserAndClient(clientId,clientSecret,username,password);

        return null;
    }

    @PostMapping("/loginUser")
    public AjaxResult loginUser(){
        return AjaxResult.success("登录成功!");
    }


}
