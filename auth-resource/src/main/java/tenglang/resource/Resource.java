package tenglang.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;
import spring.security.jwt.JwtTokenConfig;
import tenglang.common.datasource.DruidAutoConfiguration;
import tenglang.common.redis.RedisConfig;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@Import({JwtTokenConfig.class,RedisConfig.class, DruidAutoConfiguration.class})
public class Resource {
    public static void main(String[] args) {
        SpringApplication.run(Resource.class, args);
    }
}
