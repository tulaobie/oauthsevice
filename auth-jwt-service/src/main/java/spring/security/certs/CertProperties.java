package spring.security.certs;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "jwt.cert.properties")
public class CertProperties {
    /**
     * 证书后缀
     */
    private String certFileSuffix;

    /**
     * 证书密码
     */
    private String password;

    /**
     * 证书路径
     */
    private String fileStorePath;


    public String getCertFileSuffix() {
        return certFileSuffix;
    }

    public void setCertFileSuffix(String certFileSuffix) {
        this.certFileSuffix = certFileSuffix;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFileStorePath() {
        return fileStorePath;
    }

    public void setFileStorePath(String fileStorePath) {
        this.fileStorePath = fileStorePath;
    }
}
