package spring.security.certs;

import sun.security.rsa.RSAPublicKeyImpl;

import java.io.FileInputStream;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class ReadCER {
    public static void main(String[] args) {
        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            FileInputStream fileInputStream = new FileInputStream("C:\\Users\\wangj\\Desktop\\certs\\public_test.cer");
            X509Certificate cer = (X509Certificate)certificateFactory.generateCertificate(fileInputStream);
            fileInputStream.close();

            System.out.println("读取Cer证书信息...");
            System.out.println("cer_序列号___:"+cer.getSerialNumber());
            System.out.println("cer_发布方标识名___:"+cer.getIssuerDN().getName());
            System.out.println("cer_主体标识___:"+cer.getSubjectDN());
            System.out.println("cer_证书算法OID字符串___:"+cer.getSigAlgOID());
            System.out.println("cer_证书有效期___:" + cer.getNotBefore() + "~" + cer.getNotAfter());
            System.out.println("cer_签名算法___:"+cer.getSigAlgName());
            System.out.println("cer_版本号___:"+cer.getVersion());
            System.out.println("cer_公钥___:"+cer.getPublicKey());


            RSAPublicKeyImpl publicKey =(RSAPublicKeyImpl)cer.getPublicKey();
            System.out.println("系数："+publicKey.getModulus()+"  加密指数："+publicKey.getPublicExponent());




        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
