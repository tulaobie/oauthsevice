# spring-boot+nacos+spring-oauth2整合方案

#### 介绍
微服务spring-boot和nacos的整合，结合开源项目若依前后端分离项目、以及自己研发的一些通用组件等等。项目包含zuul的整合、spring-oauth2单点登录、资源服务的整合、基于ras加密协议的jwt令牌方案整合等等。本项目中包含一些来自若依的通用组件


#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/0302/172434_ac9efeb2_1050637.png "nacos微服务运转.png")

系统目前采用单机docker模式进行部署。所以需要事先安装和部署docker.

#### 基本技术栈
 ##1.docker 本系统采用docker容器化部署，其他跟window部署也没有太大区别，自行解决把
 2.spring-boot
 3.spring-oauth2 单点登录服务
 4.nacos
 5.spring-boot-redis
 6.spring-boot-mybatis
 7.spring-boot-mybatis-plus 可以事先了解一下mybatis-plus
 8.spring-boot+nacos的集成
 9.spring-boot+nacos+feign的集成

#### 安装教程
1.安装和运行docker.分别安装redis、mysql.创建桥接网络iot
2.安装nacos.文档地址:http://dubbo.apache.org/zh-cn/docs/user/references/registry/nacos.html.单机启动时需要加参数:-Dnacos.standalone=true

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request