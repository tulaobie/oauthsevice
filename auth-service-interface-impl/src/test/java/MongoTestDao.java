import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongoTestDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 根据用户名查询对象
     * @return
     */
    public MongoTestPo findTestByName(String name) {
        Query query=new Query(Criteria.where("name").is(name));
        MongoTestPo mgt =  mongoTemplate.findOne(query , MongoTestPo.class);
        return mgt;
    }


    /**
     * 根据用户名查询对象
     * @return
     */
    public List<MongoTestPo> findAll() {
        List<MongoTestPo> mgt =  mongoTemplate.findAll(MongoTestPo.class);
        return mgt;
    }

    /**
     * 更新对象
     */
    public void updateTest(MongoTestPo test) {
        Query query=new Query(Criteria.where("id").is(test.getId()));
        Update update= new Update().set("age", test.getAge()).set("name", test.getName());
        //更新查询返回结果集的第一条
        mongoTemplate.updateFirst(query,update,MongoTestPo.class);
        //更新查询返回结果集的所有
        // mongoTemplate.updateMulti(query,update,TestEntity.class);
    }

    /**
     * 删除对象
     * @param id
     */
    public void deleteTestById(Integer id) {
        Query query=new Query(Criteria.where("id").is(id));
        mongoTemplate.remove(query,MongoTestPo.class);
    }


    /**
     * 创建对象
     */
    public void saveTest2(MongoTestWitthId test) {
        mongoTemplate.save(test);
    }


    public void deleteTest2(Long id) {
        Query query=new Query(Criteria.where("id").is(id));
        mongoTemplate.remove(query,MongoTestWitthId.class);
    }


    public void saveTest(MongoTestPo test) {
        mongoTemplate.save(test);
    }


}
