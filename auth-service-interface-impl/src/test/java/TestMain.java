import com.baomidou.mybatisplus.core.metadata.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class TestMain {

    public static void main(String[] args) {
        String orderStr= "id    asc,orderNum desc";
        String orderStrArr[]=orderStr.split(",");

        List<OrderItem> orderItemList=new ArrayList<OrderItem>();
        for(String orderItem:orderStrArr){
            String ss[]=orderItem.trim().split("\\s+",2);
            System.out.println(ss[0]+"---"+ss[1]);
        }
        System.out.println();
    }
}
