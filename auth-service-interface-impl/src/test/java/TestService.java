import auth.server.impl.Application;
import auth.server.impl.dao.OauthClientDetailsDao;
import auth.server.impl.pojo.VistorRecordVo;
import auth.server.impl.service.OauthClientDetailsSerivce;
import auth.server.impl.pojo.OauthClientDetailsVo;
import auth.server.impl.service.VistorRecordService;
import auth.server.inter.pojo.ClientLogin;
import auth.server.inter.security.VistorRecord;
import auth.server.inter.service.Oauth2Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
public class TestService {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private OauthClientDetailsSerivce oauthClientDetailsSerivce;


    @Autowired
    private Oauth2Service oauth2Service;

    @Autowired
    private VistorRecordService vistorRecordService;


//    @Test
    public void testSave(){
        OauthClientDetailsVo oauthClientDetails=new OauthClientDetailsVo();
        oauthClientDetails.setClientId("test_002");
        oauthClientDetails.setName("wangjun");
        oauthClientDetails.setEnabled(true);
        oauthClientDetails.setClientSecret("111111111111111111");
        oauthClientDetailsSerivce.saveOrUpdate(oauthClientDetails);
        System.out.println("开始测试-----------------");
    }


//    @Test
    public void vistorRecordInsert(){
        VistorRecordVo vistorRecordVo=new VistorRecordVo();
        vistorRecordVo.setId(1L);
        vistorRecordVo.setContentType("application/json");
        vistorRecordVo.setAuth(true);
        vistorRecordService.saveOrUpdate(vistorRecordVo);

    }

//    @Test
    public void testFind(){
//        OauthClientDetailsVo clientDetailsVo=oauthClientDetailsSerivce.getById("client");
//        System.out.println(clientDetailsVo.getClientId());
        QueryWrapper<OauthClientDetailsVo> wrapper = new QueryWrapper<OauthClientDetailsVo>();
        wrapper.eq("name","wangjun");
        wrapper.in("client_id","test_002");
        List<OauthClientDetailsVo> dataList=oauthClientDetailsSerivce.list(wrapper);
        System.out.println(dataList.size());



//
//        QueryWrapper<OauthClientDetailsVo> queryWrapper=new QueryWrapper<OauthClientDetailsVo>();
//        queryWrapper.lambda().eq(OauthClientDetailsVo::getEnabled,true);
//        List<OauthClientDetailsVo> oauthClientDetailsVoList=oauthClientDetailsSerivce.list(queryWrapper);
//        System.out.println(oauthClientDetailsVoList.size());

//        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
//        Calendar calendar=Calendar.getInstance();
//        calendar.set(Calendar.YEAR,2018);
//        int num=1000;
//        while(num>0){
//            jdbcTemplate.update("insert into test(data) values(?)", DateFormatUtils.format(calendar,"yyyyMMdd"));
//            calendar.add(Calendar.DATE,1);
//            num--;
//        }
    }

    @Test
    public void testClient(){
        ClientLogin source=oauth2Service.loginByUserAndClient("innerClient","secret","admin","secret");
        System.out.println(source.getRefreshToken()+"----"+source.getTokenType()+"-----"+source.getAccessToken());
    }



}
