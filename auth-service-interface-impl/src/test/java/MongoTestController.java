import auth.server.impl.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(classes= Application.class)
@Import({MongoTestDao.class})
public class MongoTestController {

    @Autowired
    private MongoTestDao mtdao;

//    @Test
    public void saveTest() throws Exception {
        MongoTestPo mgtest=new MongoTestPo();
        mgtest.setId(21);
        mgtest.setAge(33);
        mgtest.setName("ceshi");
        mtdao.saveTest(mgtest);
    }


//    @Test
    public void saveTest2() throws Exception {
        MongoTestWitthId mgtest=new MongoTestWitthId();
        mgtest.setId(100L);
        mgtest.setDescription("1111111111111");
        mgtest.setBy("1111");
        mgtest.setTitle("AAAAAAAAAAAAAAAAA");
        mgtest.setUrl("www");
        mtdao.saveTest2(mgtest);
    }


//    @Test
    public void updateTest2() throws Exception {
        MongoTestWitthId mgtest=new MongoTestWitthId();
        mgtest.setId(100L);
        mgtest.setDescription("1111111111111");
        mgtest.setBy("1111");
        mgtest.setTitle("BBBBBBBBBB");
        mgtest.setUrl("www");
        mtdao.saveTest2(mgtest);
    }

    @Test
    public void deleteTest2() throws Exception {
        mtdao.deleteTest2(100L);
    }


    public MongoTestPo findTestByName(){
        MongoTestPo mgtest= mtdao.findTestByName("ceshi");
        System.out.println("mgtest is "+mgtest);
        return mgtest;
    }


//    @Test
    public void findAll(){
        List<MongoTestPo> mgtest= mtdao.findAll();
        for(MongoTestPo mongoTestPo:mgtest){
            System.out.println("mgtest is "+mongoTestPo);
        }
    }

    public void updateTest(){
        MongoTestPo mgtest=new MongoTestPo();
        mgtest.setId(11);
        mgtest.setAge(44);
        mgtest.setName("ceshi2");
        mtdao.updateTest(mgtest);
    }

    public void deleteTestById(){
        mtdao.deleteTestById(11);
    }
}
