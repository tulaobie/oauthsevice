package auth.server.impl.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScans({
        @ComponentScan({"auth.server.impl"})
})
@MapperScan(basePackages = {"auth.server.impl.dao"})
public class AuthServiceConfig {

}
