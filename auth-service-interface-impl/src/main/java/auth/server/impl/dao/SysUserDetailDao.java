package auth.server.impl.dao;

import auth.server.impl.pojo.SysUserVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface SysUserDetailDao extends BaseMapper<SysUserVo> {
}
