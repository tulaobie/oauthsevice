package auth.server.impl.dao;

import auth.server.impl.pojo.OauthClientDetailsVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface OauthClientDetailsDao extends BaseMapper<OauthClientDetailsVo> {
}
