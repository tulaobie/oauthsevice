package auth.server.impl.dao;

import auth.server.impl.pojo.VistorRecordVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface VistorRecordDao extends BaseMapper<VistorRecordVo>{
}
