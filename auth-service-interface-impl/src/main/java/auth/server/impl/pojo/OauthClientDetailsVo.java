package auth.server.impl.pojo;

import auth.server.inter.pojo.OauthClientDetails;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * @Description  
 * @Author  idea
 * @Date 2019-12-26 
 */
@TableName("oauth_client_details")
public class OauthClientDetailsVo extends OauthClientDetails implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private String clientId;


    @TableField(exist = false)
    private String resourceIds;


    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String getResourceIds() {
        return resourceIds;
    }

    @Override
    public void setResourceIds(String resourceIds) {
        this.resourceIds = resourceIds;
    }
}
