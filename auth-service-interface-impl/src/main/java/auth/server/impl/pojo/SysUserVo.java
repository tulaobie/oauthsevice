package auth.server.impl.pojo;

import auth.server.inter.pojo.SysUser;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.List;


@TableName("sys_user")
public class SysUserVo extends SysUser implements Serializable {

     @TableId(type = IdType.AUTO)
     private Long userId;


    /** 角色组 */
    @TableField(exist=false)
    private Long[] roleIds;

    /** 岗位组 */
    @TableField(exist=false)
    private Long[] postIds;

    @TableField(exist=false)
    private String[] roleKeys;

    @TableField(exist=false)
    private String[] postKeys;


    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public Long[] getRoleIds() {
        return roleIds;
    }

    @Override
    public void setRoleIds(Long[] roleIds) {
        this.roleIds=roleIds;
        super.setRoleIds(roleIds);
    }

    @Override
    public String[] getRoleKeys() {
        return super.getRoleKeys();
    }

    @Override
    public void setRoleKeys(String[] roleKeys) {
        super.setRoleKeys(roleKeys);
    }

    @Override
    public String[] getPostKeys() {
        return super.getPostKeys();
    }

    @Override
    public void setPostKeys(String[] postKeys) {
        super.setPostKeys(postKeys);
    }

    @Override
    public Long[] getPostIds() {
        return this.postIds;
    }

    @Override
    public void setPostIds(Long[] postIds) {
        this.postIds=postIds;
        super.setPostIds(postIds);
    }
}
