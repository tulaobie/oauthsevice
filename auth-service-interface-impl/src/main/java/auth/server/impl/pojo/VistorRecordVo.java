package auth.server.impl.pojo;

import auth.server.inter.security.VistorRecord;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.node.ObjectNode;
import tenglang.common.jackson.BeanUtils;

import java.sql.Timestamp;

@TableName("sys_vistor_record")
public class VistorRecordVo extends VistorRecord {


    @TableField(exist=false)
    private ObjectNode paramObject;

    @TableId
    private Long id;

    @TableField(value = "request_source")
    public String paramObjectSrc;


    @TableField(value = "response_source")
    private String responseSource;


    @TableField(value = "insert_time")
    private Timestamp insertTime;

    @TableField(value = "update_time",update = "now()")
    private Timestamp updateTime;


    public VistorRecordVo() {
    }


    public VistorRecordVo(VistorRecord vistorRecord) {
        BeanUtils.copyProperties(vistorRecord,this,"paramObject","updateTime","insertTime");
        if(vistorRecord.getParamObject()!=null){
            this.setParamObjectSrc(vistorRecord.getParamObject().toString());
        }
        this.setInsertTime(new Timestamp(System.currentTimeMillis()));
        this.setUpdateTime(new Timestamp(System.currentTimeMillis()));
    }

    @Override
    public ObjectNode getParamObject() {
        return paramObject;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id=id;
    }

    @Override
    public void setParamObject(ObjectNode paramObject) {
        this.paramObject = paramObject;
    }

    public String getParamObjectSrc() {
        return paramObjectSrc;
    }

    public void setParamObjectSrc(String paramObjectSrc) {
        this.paramObjectSrc = paramObjectSrc;
    }

    @Override
    public String getResponseSource() {
        return responseSource;
    }

    @Override
    public void setResponseSource(String responseSource) {
        this.responseSource = responseSource;
    }

    @Override
    public Timestamp getInsertTime() {
        return super.getInsertTime();
    }

    @Override
    public void setInsertTime(Timestamp insertTime) {
        super.setInsertTime(insertTime);
    }

    @Override
    public Timestamp getUpdateTime() {
        return super.getUpdateTime();
    }

    @Override
    public void setUpdateTime(Timestamp updateTime) {
        super.setUpdateTime(updateTime);
    }
}
