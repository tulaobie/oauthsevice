package auth.server.impl.service.impl;

import auth.server.impl.dao.SysUserDetailDao;
import auth.server.impl.pojo.SysUserVo;
import auth.server.inter.pojo.SysMenu;
import auth.server.inter.pojo.SysUser;
import auth.server.inter.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserDetailDao, SysUserVo> implements UserService {

    @Override
    public List<SysUser> findAllEnableUsers() {
        QueryWrapper<SysUserVo> sysUserVoQueryWrapper=new QueryWrapper<SysUserVo>();
        sysUserVoQueryWrapper.lambda().eq(SysUserVo::getDelFlag,"0").eq(SysUserVo::getStatus,"0");
        List<SysUserVo> sysUserVoList=this.list(sysUserVoQueryWrapper);
        List<SysUser> returnList=new ArrayList<>();
        for (SysUserVo sysUserVo:sysUserVoList){
            SysUser sysUser=new SysUser();
            BeanUtils.copyProperties(sysUserVo,sysUser);
            returnList.add(sysUser);
        }
        return returnList;
    }

    @Override
    public SysUser findUserByUsername(String userName) {
        QueryWrapper<SysUserVo> sysUserVoQueryWrapper=new QueryWrapper<SysUserVo>();
        sysUserVoQueryWrapper.lambda().eq(SysUserVo::getUserName,userName);
        SysUserVo sysUserVo=this.getOne(sysUserVoQueryWrapper);
        if(sysUserVo!=null){
            SysUser sysUser=new SysUser();
            BeanUtils.copyProperties(sysUserVo,sysUser);
            return sysUser;
        }
        return null;
    }

    @Override
    public List<SysMenu> findMenuByUsername(String userName) {
        return null;
    }
}
