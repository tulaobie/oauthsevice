package auth.server.impl.service.impl;

import auth.server.impl.pojo.OauthClientDetailsVo;
import auth.server.impl.service.OauthClientDetailsSerivce;
import auth.server.impl.dao.OauthClientDetailsDao;
import auth.server.inter.pojo.OauthClientDetails;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("oauthClientDetailsSerivce")
@Transactional
public class OauthClientDetailsSerivceImpl extends ServiceImpl<OauthClientDetailsDao, OauthClientDetailsVo> implements OauthClientDetailsSerivce {

    /**
     * 获取所有的连接端
     *
     * @return
     */
    @Override
    public List<OauthClientDetails> findAllEnableClients() {
        QueryWrapper<OauthClientDetailsVo> queryWrapper=new QueryWrapper<OauthClientDetailsVo>();
        queryWrapper.lambda().eq(OauthClientDetailsVo::getEnabled,true);
        List<OauthClientDetailsVo> oauthClientDetailsVoList=this.list(queryWrapper);
        List<OauthClientDetails> oauthClientDetailsList=new ArrayList<OauthClientDetails>();
        for (OauthClientDetailsVo oauthClientDetailsVo:oauthClientDetailsVoList){
            OauthClientDetails oauthClientDetails=new OauthClientDetails();
            BeanUtils.copyProperties(oauthClientDetailsVo,oauthClientDetails);
            oauthClientDetailsList.add(oauthClientDetails);
        }
        return oauthClientDetailsList;
    }

    /**
     * 通过clientId获取莫一端
     *
     * @param clientId
     * @return
     */
    @Override
    public OauthClientDetails findClientsByClientId(String clientId) {
        QueryWrapper<OauthClientDetailsVo> queryWrapper=new QueryWrapper<OauthClientDetailsVo>();
        queryWrapper.lambda().eq(OauthClientDetailsVo::getClientId,clientId);
        OauthClientDetailsVo  oauthClientDetailsVo= this.getOne(queryWrapper);
        OauthClientDetails oauthClientDetails=new OauthClientDetails();
        BeanUtils.copyProperties(oauthClientDetailsVo,oauthClientDetails);
        return oauthClientDetails;
    }
}
