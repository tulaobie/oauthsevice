package auth.server.impl.service;

import auth.server.inter.pojo.OauthClientDetails;
import auth.server.inter.service.ClientService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Override
    public List<OauthClientDetails> findAllEnableClients() {
        return null;
    }

    @Override
    public OauthClientDetails findClientsByClientId(String clientId) {
        return null;
    }
}
