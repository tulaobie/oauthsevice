package auth.server.impl.service;

import auth.server.impl.pojo.OauthClientDetailsVo;
import auth.server.inter.service.ClientService;
import com.baomidou.mybatisplus.extension.service.IService;

public interface OauthClientDetailsSerivce extends IService<OauthClientDetailsVo>, ClientService {

}
