package auth.server.impl.service.impl;

import auth.server.impl.dao.VistorRecordDao;
import auth.server.impl.pojo.VistorRecordVo;
import auth.server.impl.service.VistorRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class VistorRecordServiceImpl extends ServiceImpl<VistorRecordDao, VistorRecordVo> implements VistorRecordService {
}
