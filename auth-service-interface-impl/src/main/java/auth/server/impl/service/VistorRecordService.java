package auth.server.impl.service;

import auth.server.impl.pojo.VistorRecordVo;
import com.baomidou.mybatisplus.extension.service.IService;

public interface VistorRecordService extends IService<VistorRecordVo> {
}
