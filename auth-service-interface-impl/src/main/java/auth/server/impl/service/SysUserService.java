package auth.server.impl.service;

import auth.server.impl.pojo.SysUserVo;
import auth.server.inter.service.UserService;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysUserService  extends IService<SysUserVo>, UserService {
}
