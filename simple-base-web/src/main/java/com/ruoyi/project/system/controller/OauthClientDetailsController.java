package com.ruoyi.project.system.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.project.system.domain.OauthClientDetails;
import com.ruoyi.project.system.service.IOauthClientDetailsService;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import tenglang.common.web.controller.BaseController;
import tenglang.common.web.domain.AjaxResult;
import tenglang.common.web.page.TableDataInfo;

/**
 * 服务终端管理Controller
 *
 * @author ruoyi
 * @date 2020-02-29
 */
@RestController
@RequestMapping("/system/client")
@Api(tags = "服务终端管理接口服务")
public class OauthClientDetailsController extends BaseController {
    @Autowired
    private IOauthClientDetailsService oauthClientDetailsService;

    /**
     * 查询服务终端管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:client:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询服务终端管理列表", notes = "查询服务终端管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "clientId", value = "客户端编号", dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "enabled", value = "是否启用", dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "pageNum", value = "当前页数", dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "pageSize", value = "每页记录数", dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "orderBy", value = "排序字段", dataType = "String")
    })
    public TableDataInfo list(@RequestParam(name = "clientId") String clientId, @RequestParam(name = "enabled") Integer enabled, @RequestParam(name = "pageNum", required = true, defaultValue = "1") Integer pageNum,
                              @RequestParam(name = "pageSize", required = true) Integer pageSize
    ) {
        startPage();
        Map<String, Object> queryMap = new HashMap();
        queryMap.put("clientId", clientId);
        queryMap.put("enabled", enabled);
        List<OauthClientDetails> list = oauthClientDetailsService.selectList(queryMap);
        return getDataTable(list);
    }


    /**
     * 获取服务终端管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:client:query')")
    @GetMapping(value = "/{clientId}")
    @ApiOperation(value = "获取服务终端管理详细信息", notes = "获取服务终端管理详细信息")
    public AjaxResult getInfo(@PathVariable("clientId") String clientId) {
        return AjaxResult.success(oauthClientDetailsService.getById(clientId));
    }

    /**
     * 新增服务终端管理
     */
    @PreAuthorize("@ss.hasPermi('system:client:add')")
    @PostMapping
    @ApiOperation(value = "新增服务终端管理", notes = "新增服务终端管理")
    public AjaxResult add(@RequestBody OauthClientDetails oauthClientDetails) {
        return toAjax(oauthClientDetailsService.save(oauthClientDetails));
    }

    /**
     * 修改服务终端管理
     */
    @PreAuthorize("@ss.hasPermi('system:client:edit')")
    @PutMapping
    @ApiOperation(value = "修改服务终端管理", notes = "修改服务终端管理")
    public AjaxResult edit(@RequestBody OauthClientDetails oauthClientDetails) {
        return toAjax(oauthClientDetailsService.saveOrUpdate(oauthClientDetails));
    }

    /**
     * 删除服务终端管理
     */
    @PreAuthorize("@ss.hasPermi('oauthClientDetails:client:remove')")
    @DeleteMapping("/{clientIds}")
    @ApiOperation(value = "删除服务终端管理", notes = "删除服务终端管理")
    public AjaxResult remove(@PathVariable String[] clientIds) {
        return toAjax(oauthClientDetailsService.removeByIds(Arrays.asList(clientIds)));
    }
}
