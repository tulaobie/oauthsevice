package com.ruoyi.project.system.service;

import com.ruoyi.project.system.domain.OauthClientDetails;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 服务终端管理Service接口
 * 
 * @author ruoyi
 * @date 2020-02-29
 */
public interface IOauthClientDetailsService extends  IService<OauthClientDetails>
{
    public List<OauthClientDetails> selectList(Map<String, Object> oauthClientDetails);
}
