package com.ruoyi.project.system.mapper;

import com.ruoyi.project.system.domain.OauthClientDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 服务终端管理Mapper接口
 * 
 * @author ruoyi
 * @date 2020-02-29
 */
public interface OauthClientDetailsMapper extends BaseMapper<OauthClientDetails>
{

}
