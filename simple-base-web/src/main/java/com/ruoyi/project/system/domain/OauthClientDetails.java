package com.ruoyi.project.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import tenglang.common.web.domain.BaseEntity;


/**
 * 服务终端管理对象 oauth_client_details
 *
 * @author ruoyi
 * @date 2020-02-29
 */
@ApiModel("服务终端管理")
public class OauthClientDetails extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 客户端编号
     */
    @ApiModelProperty(value = "客户端编号")
    private String clientId;

    /**
     * 客户端密码
     */
    @ApiModelProperty(value = "客户端编号")
    private String clientSecret;

    /**
     * 作用域
     */
    @ApiModelProperty(value = "作用域")
    private String scope;

    /**
     * 授权方式
     */
    @ApiModelProperty(value = "授权方式")
    private String authorizedGrantTypes;

    /**
     * 跳转URL
     */
    @ApiModelProperty(value = "授权方式")
    private String webServerRedirectUri;

    /**
     * 客户端默认授权角色
     */
    @ApiModelProperty(value = "授权方式")
    private String authorities;

    /**
     * token有效期
     */
    @ApiModelProperty(value = "token有效期")
    private Long accessTokenValidity;

    /**
     * 刷新token有效时间
     */
    @ApiModelProperty(value = "刷新token有效时间")
    private Long refreshTokenValidity;

    /**
     * 其他token附件信息
     */
    @ApiModelProperty(value = "刷新token有效时间")
    private String additionalInformation;

    /**
     * 是否自动确认
     */
    @ApiModelProperty(value = "是否自动确认")
    private Integer autoApprove;

    /**
     * 客户端名称
     */
    @ApiModelProperty(value = "客户端名称")
    private String name;

    /**
     * $column.columnComment
     */
    @ApiModelProperty(value = "客户端名称")
    private Integer enabled;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getScope() {
        return scope;
    }

    public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
        this.authorizedGrantTypes = authorizedGrantTypes;
    }

    public String getAuthorizedGrantTypes() {
        return authorizedGrantTypes;
    }

    public void setWebServerRedirectUri(String webServerRedirectUri) {
        this.webServerRedirectUri = webServerRedirectUri;
    }

    public String getWebServerRedirectUri() {
        return webServerRedirectUri;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    public String getAuthorities() {
        return authorities;
    }

    public void setAccessTokenValidity(Long accessTokenValidity) {
        this.accessTokenValidity = accessTokenValidity;
    }

    public Long getAccessTokenValidity() {
        return accessTokenValidity;
    }

    public void setRefreshTokenValidity(Long refreshTokenValidity) {
        this.refreshTokenValidity = refreshTokenValidity;
    }

    public Long getRefreshTokenValidity() {
        return refreshTokenValidity;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAutoApprove(Integer autoApprove) {
        this.autoApprove = autoApprove;
    }

    public Integer getAutoApprove() {
        return autoApprove;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Integer getEnabled() {
        return enabled;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("clientId", getClientId())
                .append("clientSecret", getClientSecret())
                .append("scope", getScope())
                .append("authorizedGrantTypes", getAuthorizedGrantTypes())
                .append("webServerRedirectUri", getWebServerRedirectUri())
                .append("authorities", getAuthorities())
                .append("accessTokenValidity", getAccessTokenValidity())
                .append("refreshTokenValidity", getRefreshTokenValidity())
                .append("additionalInformation", getAdditionalInformation())
                .append("autoApprove", getAutoApprove())
                .append("name", getName())
                .append("enabled", getEnabled())
                .toString();
    }
}
