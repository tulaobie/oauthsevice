package com.ruoyi.project.system.service.impl;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.ruoyi.project.system.mapper.OauthClientDetailsMapper;
import com.ruoyi.project.system.domain.OauthClientDetails;
import com.ruoyi.project.system.service.IOauthClientDetailsService;
import tenglang.common.utils.StringUtils;

/**
 * 服务终端管理Service业务层处理
 *
 * @author ruoyi
 * @date 2020-02-29
 */
@Service
public class OauthClientDetailsServiceImpl extends ServiceImpl<OauthClientDetailsMapper, OauthClientDetails> implements IOauthClientDetailsService {
    @Autowired
    private OauthClientDetailsMapper oauthClientDetailsMapper;


    /**
     * 查询服务终端管理列表
     *
     * @param oauthClientDetails 服务终端管理
     * @return 服务终端管理
     */
    @Override
    public List<OauthClientDetails> selectList(Map<String, Object> oauthClientDetails) {
        QueryWrapper<OauthClientDetails> wrapper = new QueryWrapper<OauthClientDetails>();
        if (StringUtils.isNotBlank((String)oauthClientDetails.get("clientId"))) {
            wrapper.eq("client_id", oauthClientDetails.get("clientId"));
        }
        if (oauthClientDetails.get("enabled") != null) {
            wrapper.like("enabled", "%" + oauthClientDetails.get("enabled") + "%");
        }
        return this.list(wrapper);
    }


}
