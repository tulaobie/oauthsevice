package com.ruoyi.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import spring.security.jwt.JwtTokenConfig;
import tenglang.common.datasource.DruidAutoConfiguration;
import tenglang.common.httpclient.HttpClientFeignConfiguration;
import tenglang.common.redis.RedisConfig;
import tenglang.resource.config.ResourceServerConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@Import({JwtTokenConfig.class,RedisConfig.class, DruidAutoConfiguration.class, ResourceServerConfiguration.class, HttpClientFeignConfiguration.class})
@MapperScan(basePackages = {"com.ruoyi.project.system.mapper"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages={"auth.server.inter.service"})
public class Resource {
    public static void main(String[] args) {
        SpringApplication.run(Resource.class, args);
    }
}
