package tenglang.common;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import tenglang.common.datasource.DruidAutoConfiguration;
import tenglang.common.httpclient.HttpClientFeignConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class, FeignAutoConfiguration.class, FeignRibbonClientAutoConfiguration.class, MongoAutoConfiguration.class})
@Import(DruidAutoConfiguration.class)
@EnableDiscoveryClient
@ComponentScan(excludeFilters = {@ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE,value = HttpClientFeignConfiguration.class)})
//@EnableFeignClients(basePackages={"auth.server.inter.service"})
//@MapperScan(basePackages = {"mybatis"})
public class MybatisTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisTestApplication.class, args);
    }
}
