/**   
* @Title: RedisUtils.java 
* @Package spring.webscoket.common.redis 
* @Description: TODO(用一句话描述该文件做什么) 
* @author wangjun
* @date 2018年10月31日 下午5:44:53 
* @version V1.0   
*/
package tenglang.common.redis;

import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/** 
* @ClassName: RedisUtils 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author wangjun
* @date 2018年10月31日 下午5:44:53 
*  
*/
public class RedisUtils {
      
	  /**
	   * 获取以一个30s的周期
	   * @return
	   */
	  public static String getCurrentTaskGroupName(){
		  Long modeValue=1000*30L;
		  Date nowDate=new Date();
		  Long key=nowDate.getTime()-(nowDate.getTime()%modeValue);
		  return String.valueOf(key);
	  }
	  
	  /**
	   * 获取以一个20s的周期
	   * @return
	   */
	  public static String getHistoryTaskGroupName(Date d){
		  Long modeValue=1000*30L;
		  Long key=d.getTime()-(d.getTime()%modeValue);
		  return String.valueOf(key);
	  }
	  
	  /**
	   * 获取一个指定分钟数的周期
	   * @return
	   */
	  public static String getMinuteStoreGroup(int minute,Date d) {
		  Long modeValue=1000*60L*minute;
		  Date nowDate=(d==null)?new Date():d;
		  Long key=nowDate.getTime()-(nowDate.getTime()%modeValue);
		  return String.valueOf(key);
	  }
	  
	  /**
	   * 获取当前周期指定的周期后的一个旧的周期
	   * @return
	   */
	  public static String getMinuteStoreGroup(int minute,int afterNumCycle,Date d) {
		  Long modeValue=1000*60L*minute;
		  Date nowDate=(d==null)?new Date():d;
		  Long key=nowDate.getTime()-(nowDate.getTime()%modeValue);
		  key=key-modeValue*afterNumCycle;
		  return String.valueOf(key);
	  }
	  
	  
	  public static String getMinuteStoreGroup(int minute,int afterNumCycle) {
		  Long modeValue=1000*60L*minute;
		  Date nowDate=new Date();
		  Long key=nowDate.getTime()-(nowDate.getTime()%modeValue);
		  key=key-modeValue*afterNumCycle;
		  return String.valueOf(key);
	  }
	  
	  public static List<String> getMinuteStoreGroupsRang(int minute,Date begin,Date end){
		  List<String> sets=new ArrayList();
		  String beginGoup=getMinuteStoreGroup(minute,0,begin);
		  sets.add(beginGoup);
		  Calendar calendar=Calendar.getInstance();
		  calendar.setTimeInMillis(Long.parseLong(beginGoup));;
		  calendar.add(Calendar.MINUTE, minute);
		  while(calendar.getTimeInMillis()<end.getTime()) {
			  String s=getMinuteStoreGroup(minute, 0, calendar.getTime());
			  sets.add(s);
			  calendar.add(Calendar.MINUTE, minute);
		  }
		  return sets;
	  }
	  
	  public static String getMinuteStoreGroup(int minute) {
		  return getMinuteStoreGroup(minute,null);
	  }
	   
	  
	  public static <T> List<T> splitToList(String source,Class<T> t) throws Exception{
		  String[] arr=source.split(",");
		  List<T> values=new ArrayList<T>();
		  Constructor<T> constructor=t.getConstructor(String.class);
		  for(String s:arr) {
				values.add(constructor.newInstance(s));
		  }
		  return values;
	  }
	  
	  public static String listToString(List datas) {
		  StringBuffer buffer=new StringBuffer();
		  for(Object l:datas) {
			  buffer.append(String.valueOf(l));
			  buffer.append(",");
		  }
		  if(buffer.length()>0) {
			  buffer.deleteCharAt(buffer.length()-1);
		  }
		  return buffer.toString();
	  }
     	  
	  public static short getReceiveGroupName(){
		  Date d=new Date();
		  Long modeValue=1000*20L;
		  Long key=d.getTime()-(d.getTime()%modeValue);
		  long group=(key/modeValue)%2;
		  
		  if(group==0) {
			  return 0;
		  }else {
			  return 1;
		  }
	  }
	  
	  public static void main(String[] args) throws Exception {
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		System.out.println(getMinuteStoreGroup(10));
//		
//		Calendar calendar=Calendar.getInstance();
//		calendar.add(Calendar.DAY_OF_YEAR, -6);
//		calendar.set(Calendar.HOUR, 0);
//		calendar.set(Calendar.MINUTE, 0);
//		calendar.set(Calendar.SECOND, 0);
//		calendar.set(Calendar.MILLISECOND, 0);
//		
//		System.out.println(dateFormat.format(calendar.getTime()));
//		
//		System.out.println(dateFormat.format(new Date(Long.parseLong(getMinuteStoreGroup(20, 0)))));
		
	}
}
