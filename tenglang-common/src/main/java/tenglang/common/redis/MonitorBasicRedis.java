package tenglang.common.redis;

public interface MonitorBasicRedis {

	Object get();
	Object update();
	void evict();
}
