package tenglang.common.web.page;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tenglang.common.utils.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 表格数据处理
 * 
 * @author ruoyi
 */
public class TableSupport
{
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderBy";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 封装分页对象
     */
    public static Page<T> getPlusPage()
    {
        //两个方法在没有使用JSF的项目中是没有区别的
        Page<T> pageDomain=new Page(ServletUtils.getParameterToInt(PAGE_NUM),ServletUtils.getParameterToInt(PAGE_SIZE));
        String orderStr=ServletUtils.getParameter(ORDER_BY_COLUMN);
        String orderStrArr[]=orderStr.split(",");

        List<OrderItem> orderItemList=new ArrayList<OrderItem>();
        for(String orderItem:orderStrArr){
            orderItem.trim().split("");

        }
        return pageDomain;
    }

    public  static Page<T> buildPlusPageRequest()
    {
        return getPlusPage();
    }


    public static PageDomain getPageDomain()
    {
        PageDomain pageDomain = new PageDomain();
        pageDomain.setPageNum(ServletUtils.getParameterToInt(PAGE_NUM));
        pageDomain.setPageSize(ServletUtils.getParameterToInt(PAGE_SIZE));
        pageDomain.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        pageDomain.setIsAsc(ServletUtils.getParameter(IS_ASC));
        return pageDomain;
    }

    public static PageDomain buildPageRequest()
    {
        return getPageDomain();
    }


}
