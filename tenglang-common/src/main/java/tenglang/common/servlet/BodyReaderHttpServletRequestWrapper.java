package tenglang.common.servlet;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * @author ethan
 */
public class BodyReaderHttpServletRequestWrapper extends HttpServletRequestWrapper {
 
    private final byte[] body;
 
    public BodyReaderHttpServletRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);
        body = HttpHelper.getBodyString(request).getBytes(Charset.forName("UTF-8"));
    }
 
    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }
 
    @Override
    public ServletInputStream getInputStream() throws IOException {
 
        final ByteArrayInputStream bais = new ByteArrayInputStream(body);
 
        return new ServletInputStream() {
            @Override
            public int read() throws IOException {
                return bais.read();
            }

            /**
             * Returns true when all the data from the stream has been read else
             * it returns false.
             *
             * @return <code>true</code> when all data for this particular request
             * has been read, otherwise returns <code>false</code>.
             * @since Servlet 3.1
             */
            @Override
            public boolean isFinished() {
                return bais.available() == 0;
            }

            /**
             * Returns true if data can be read without blocking else returns
             * false.
             *
             * @return <code>true</code> if data can be obtained without blocking,
             * otherwise returns <code>false</code>.
             * @since Servlet 3.1
             */
            @Override
            public boolean isReady() {
                return true;
            }

            /**
             * Instructs the <code>ServletInputStream</code> to invoke the provided
             * {@link ReadListener} when it is possible to read
             *
             * @param readListener the {@link ReadListener} that should be notified
             *                     when it's possible to read.
             * @throws IllegalStateException if one of the following conditions is true
             *                               <ul>
             *                               <li>the associated request is neither upgraded nor the async started
             *                               <li>setReadListener is called more than once within the scope of the same request.
             *                               </ul>
             * @throws NullPointerException  if readListener is null
             * @since Servlet 3.1
             */
            @Override
            public void setReadListener(ReadListener readListener) {

            }
        };
    }
}