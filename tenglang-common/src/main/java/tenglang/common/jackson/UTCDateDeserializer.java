package tenglang.common.jackson;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers.DateDeserializer;

public class UTCDateDeserializer extends DateDeserializer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(UTCDateDeserializer.class);
	

	@Override
	protected Date _parseDate(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		Date _parseDate = null;
		try {
			_parseDate = super._parseDate(jp, ctxt);
		} catch (Exception ex) {
			   String dateStr = jp.getText().trim();
			   try {
				  return DateUtils.parseDate(dateStr, new String[]{"yyyy-MM-dd","yy-MM-dd","yy.MM.dd","yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss.SSS"});
			} catch (ParseException e) {
				  return  null;
			}
		 }
		return _parseDate;
	}
	
	

}
