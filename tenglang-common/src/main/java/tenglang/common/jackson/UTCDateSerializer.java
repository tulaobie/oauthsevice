package tenglang.common.jackson;

import java.io.IOException;
import java.util.Date;


import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import org.apache.commons.lang3.time.DateFormatUtils;

public class UTCDateSerializer extends DateSerializer {
	@Override
	public void serialize(Date value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonGenerationException {
		  jgen.writeString(DateFormatUtils.format(value, "yyyy-MM-dd HH:mm:ss"));
	}
}
