package tenglang.common.httpclient;

import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class HttpConnectionPoolUtil implements FactoryBean<CloseableHttpClient> {
    private static Logger logger = LoggerFactory.getLogger(HttpConnectionPoolUtil.class);


    private HttpClientConfig httpClientConfig;

    private  CloseableHttpClient httpClient; // 发送请求的客户端单例
    private  PoolingHttpClientConnectionManager manager; //连接池管理类
    private  ScheduledExecutorService monitorExecutor;


    private final static Object syncLock = new Object(); // 相当于线程锁,用于线程安全

    public HttpConnectionPoolUtil(HttpClientConfig config){
        this.httpClientConfig=config;
        int MAX_CONN = httpClientConfig.getHttpMaxPoolSize(); // 最大连接数
        int Max_PRE_ROUTE = httpClientConfig.getHttpMaxPoolSize();
        int MAX_ROUTE = httpClientConfig.getHttpMaxPoolSize();
        String defaultUrl=httpClientConfig.getDefaultUrl();

        ConnectionSocketFactory plainSocketFactory = PlainConnectionSocketFactory.getSocketFactory();
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }

                    public void checkServerTrusted(X509Certificate[] arg0,
                                                   String arg1) throws CertificateException {

                    }

                    public void checkClientTrusted(X509Certificate[] arg0,
                                                   String arg1) throws CertificateException {

                    }
                }};
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                //return hostname.equals(session.getPeerHost());
                return true;
            }
        };
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            LayeredConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sc,hv);
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create().register("http", plainSocketFactory)
                    .register("https", sslSocketFactory).build();
            manager = new PoolingHttpClientConnectionManager(registry);

            //设置连接参数
            manager.setMaxTotal(MAX_CONN); // 最大连接数
            manager.setDefaultMaxPerRoute(Max_PRE_ROUTE); // 路由最大连接数
//            HttpHost httpHost = HttpHost.create(defaultUrl);
//            manager.setMaxPerRoute(new HttpRoute(httpHost), MAX_ROUTE);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    /**
     * 对http请求进行基本设置
     * @param httpRequestBase http请求
     */
    public  static void setRequestConfig(HttpRequestBase httpRequestBase,HttpClientConfig httpClientConfig){
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(httpClientConfig.getHttpConnectTimeout())
                .setConnectTimeout(httpClientConfig.getHttpConnectTimeout())
                .setSocketTimeout(httpClientConfig.getHttpSocketTimeout()).build();
        httpRequestBase.setConfig(requestConfig);
    }

    public  CloseableHttpClient getHttpClient(){
        if (httpClient == null){
            //多线程下多个线程同时调用getHttpClient容易导致重复创建httpClient对象的问题,所以加上了同步锁
            synchronized (syncLock){
                if (httpClient == null){
                    httpClient = createHttpClient();
                    //开启监控线程,对异常和空闲线程进行关闭
                    monitorExecutor = Executors.newScheduledThreadPool(1);
                    monitorExecutor.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            //关闭异常连接
                            manager.closeExpiredConnections();
                            //关闭5s空闲的连接
                            manager.closeIdleConnections(httpClientConfig.getHttpIdelTimeout(), TimeUnit.MILLISECONDS);
                        }
                    }, httpClientConfig.getHttpMonitorInterval(), httpClientConfig.getHttpMonitorInterval(), TimeUnit.MILLISECONDS);
                }
            }
        }
        return httpClient;
    }

    /**
     * 根据host和port构建httpclient实例
     * @param host 要访问的域名
     * @param port 要访问的端口
     * @return
     */
    public  CloseableHttpClient createHttpClient(){
        //请求失败时,进行请求重试
        HttpRequestRetryHandler handler = new HttpRequestRetryHandler() {
            @Override
            public boolean retryRequest(IOException e, int i, HttpContext httpContext) {
                if (i > 3){
                    //重试超过3次,放弃请求
                    logger.error("retry has more than 3 time, give up request");
                    return false;
                }
                if (e instanceof NoHttpResponseException){
                    //服务器没有响应,可能是服务器断开了连接,应该重试
                    logger.error("receive no response from server, retry");
                    return true;
                }
                if (e instanceof SSLHandshakeException){
                    // SSL握手异常
                    logger.error("SSL hand shake exception");
                    return false;
                }
                if (e instanceof InterruptedIOException){
                    //超时
                    logger.error("InterruptedIOException");
                    return false;
                }
                if (e instanceof UnknownHostException){
                    // 服务器不可达
                    logger.error("server host unknown");
                    return false;
                }
                if (e instanceof ConnectTimeoutException){
                    // 连接超时
                    logger.error("Connection Time out");
                    return false;
                }
                if (e instanceof SSLException){
                    logger.error("SSLException");
                    return false;
                }

                HttpClientContext context = HttpClientContext.adapt(httpContext);
                HttpRequest request = context.getRequest();
                if (!(request instanceof HttpEntityEnclosingRequest)){
                    //如果请求不是关闭连接的请求
                    return true;
                }
                return false;
            }
        };
        CloseableHttpClient client = HttpClients.custom().setConnectionManager(manager).setRetryHandler(handler).build();
        return client;
    }

    @Override
    public CloseableHttpClient getObject() throws Exception {
        return getHttpClient();
    }

    @Override
    public Class<?> getObjectType() {
        return CloseableHttpClient.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public void setHttpClientConfig(HttpClientConfig httpClientConfig) {
        this.httpClientConfig = httpClientConfig;
    }


    public static void main(String[] args) {
        HttpClientConfig config=new HttpClientConfig();
        config.setDefaultUrl("http://127.0.0.1:8080");
        HttpConnectionPoolUtil httpConnectionPoolUtil=new HttpConnectionPoolUtil(config);
        CloseableHttpClient httpClient=httpConnectionPoolUtil.getHttpClient();
        HttpGet post=new HttpGet("http://127.0.0.1:8080/lettor-web/index");
        try {
            CloseableHttpResponse response=httpClient.execute(post);
            System.out.println(EntityUtils.toString(response.getEntity(),"utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
