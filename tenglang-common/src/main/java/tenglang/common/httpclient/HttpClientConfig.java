package tenglang.common.httpclient;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "feign.httpclient",ignoreUnknownFields = true,ignoreInvalidFields = true)
@Component
public class HttpClientConfig {
    private int httpConnectTimeout = 60000;

    private int httpSocketTimeout = 60000;

    private int httpMaxPoolSize = 100;

    //定期处理空闲连接
    private int httpMonitorInterval = 3000;

    //空闲超时时间
    private int httpIdelTimeout = 2000;

    private String defaultUrl;


    public int getHttpConnectTimeout() {
        return httpConnectTimeout;
    }

    public void setHttpConnectTimeout(int httpConnectTimeout) {
        this.httpConnectTimeout = httpConnectTimeout;
    }

    public int getHttpSocketTimeout() {
        return httpSocketTimeout;
    }

    public void setHttpSocketTimeout(int httpSocketTimeout) {
        this.httpSocketTimeout = httpSocketTimeout;
    }

    public int getHttpMaxPoolSize() {
        return httpMaxPoolSize;
    }

    public void setHttpMaxPoolSize(int httpMaxPoolSize) {
        this.httpMaxPoolSize = httpMaxPoolSize;
    }

    public int getHttpMonitorInterval() {
        return httpMonitorInterval;
    }

    public void setHttpMonitorInterval(int httpMonitorInterval) {
        this.httpMonitorInterval = httpMonitorInterval;
    }

    public int getHttpIdelTimeout() {
        return httpIdelTimeout;
    }

    public void setHttpIdelTimeout(int httpIdelTimeout) {
        this.httpIdelTimeout = httpIdelTimeout;
    }

    public String getDefaultUrl() {
        return defaultUrl;
    }

    public void setDefaultUrl(String defaultUrl) {
        this.defaultUrl = defaultUrl;
    }
}
