package tenglang.common.utils;


import org.apache.commons.collections4.keyvalue.DefaultMapEntry;
import org.apache.http.util.ByteArrayBuffer;
import org.springframework.util.Assert;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtils {

    private ZipInputStream zipInputStream;
    private Map<String, byte[]> decompressionMap=new HashMap<>();

    public ZipUtils(InputStream zipFileStream){
         Assert.notNull(zipFileStream,"zipFileStream can not null!");
         this.zipInputStream=new ZipInputStream(zipFileStream, Charset.forName("GBK"));
         this.decompression();
    }


     public void  decompression(){
         ByteArrayBuffer byteArrayBuffer=new ByteArrayBuffer(1024*1024);
         ZipEntry zipEntry=null;
         try {
             while((zipEntry=zipInputStream.getNextEntry())!=null){
                 byteArrayBuffer.clear();
                 byte zb[]=new byte[1024];
                 String currentParent=null;
                  if(zipEntry.getName().endsWith("/") || zipEntry.getName().endsWith("\\")){
                      currentParent=zipEntry.getName();
                  }else{
                      int num=-1;
                      while((num=zipInputStream.read(zb,0,zb.length))>-1){
                          byteArrayBuffer.append(zb,0,num);
                      }
                      System.out.println(zipEntry.getName());
                      decompressionMap.put(zipEntry.getName(),byteArrayBuffer.toByteArray());
                      zipInputStream.closeEntry();
                 }
             }
             zipInputStream.close();
         } catch (Exception e) {
             e.printStackTrace();
         } finally {

         }
     }

     public Map.Entry<String,byte[]> decompressionKey(String key){
         Iterator<Map.Entry<String,byte[]>> decompressionIterator=decompressionMap.entrySet().iterator();
         Map.Entry<String,byte[]> decompressionData=null;
          while(decompressionIterator.hasNext()){
              Map.Entry<String,byte[]> decompressionEntry=decompressionIterator.next();
              if(decompressionEntry.getKey().indexOf(key)==-1){
                  String[] qq=decompressionEntry.getKey().split("/|\\\\");
                  decompressionData=new DefaultMapEntry<String,byte[]>(qq[qq.length-1],decompressionEntry.getValue());
              }
          }
          return decompressionData;
     }

    public Map<String,byte[]> filterKey(String prefixKey){
        Iterator<Map.Entry<String,byte[]>> decompressionIterator=decompressionMap.entrySet().iterator();
        Map<String,byte[]> returnMap=new HashMap();
        while(decompressionIterator.hasNext()){
            Map.Entry<String,byte[]> decompressionEntry=decompressionIterator.next();
            String decompressionEntryKey=decompressionEntry.getKey();
            if(decompressionEntryKey.startsWith(prefixKey)){
                String finalKey=decompressionEntryKey.substring(prefixKey.length()+1);
                if(finalKey.indexOf("/")==-1){
                    String[] qq=decompressionEntry.getKey().split("/|\\\\");
                    returnMap.put(qq[qq.length-1],decompressionEntry.getValue());
                }
            }
        }
        return returnMap;
    }

    public Map<String,byte[]> filterKeyEnd(String prefixKey){
        Iterator<Map.Entry<String,byte[]>> decompressionIterator=decompressionMap.entrySet().iterator();
        Map<String,byte[]> returnMap=new HashMap();
        while(decompressionIterator.hasNext()){
            Map.Entry<String,byte[]> decompressionEntry=decompressionIterator.next();
            String decompressionEntryKey=decompressionEntry.getKey();
            if(decompressionEntryKey.endsWith(prefixKey)){
                String finalKey=decompressionEntryKey.substring(prefixKey.length()+1);
                String[] qq=decompressionEntry.getKey().split("/|\\\\");
                returnMap.put(qq[qq.length-1],decompressionEntry.getValue());
            }
        }
        return returnMap;
    }

    public boolean containKey(String key){
        Iterator<Map.Entry<String,byte[]>> decompressionIterator=decompressionMap.entrySet().iterator();
        Map.Entry<String,byte[]> decompressionData=null;
        boolean containKey=false;
        while(decompressionIterator.hasNext()){
            Map.Entry<String,byte[]> decompressionEntry=decompressionIterator.next();
            if(decompressionEntry.getKey().indexOf(key)>-1){
                containKey=true;
            }
        }
        return containKey;
    }



    public static void main(String[] args) {
//         String s="郑东新区施工工地调查表(1)\\祭城施工工地调查表 7.9\\施工工地调查表(郑欧商务中心项目）(1)(1).doc";
//         String[] qq=s.split("/|\\\\");
//         System.out.println(qq[qq.length-1]);

        try {
            FileInputStream fileInputStream=new FileInputStream("C:\\Users\\wangj\\Downloads\\ruoyi.zip");
            ZipUtils zipUtils=new ZipUtils(fileInputStream);
            Map<String,byte[]> getEntrys=zipUtils.filterKey("main/java/");
            System.out.println(getEntrys.size());

            getEntrys=zipUtils.filterKey("main/java/com/ruoyi/project/system/domain");
            System.out.println(getEntrys.size());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }



}
