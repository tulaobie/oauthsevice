package tenglang.common.utils;

public class HexUtils {
	 /** Convert byte[] to hex string.这里我们可以将byte转换成int，然后利用Integer.toHexString(int)来转换成16进制字符串。   
	 * @param src byte[] data   
	 * @return hex string   
	 */      
	public static String bytesToHexString(byte[] src){   
	    StringBuilder stringBuilder = new StringBuilder("");   
	    if (src == null || src.length <= 0) {   
	        return null;   
	    }   
	    for (int i = 0; i < src.length; i++) {   
	        int v = src[i] & 0xFF;   
	        String hv = Integer.toHexString(v);   
	        if (hv.length() < 2) {   
	            stringBuilder.append(0);   
	        }   
	        stringBuilder.append(hv);   
	    }   
	    return stringBuilder.toString();   
	}
	
	
	/**  
	 * Convert hex string to byte[]  
	 * @param hexString the hex string  
	 * @return byte[]  
	 */  
	public static byte[] hexStringToBytes(String hexString) {   
	    if (hexString == null || hexString.equals("")) {   
	        return null;   
	    }   
	    hexString = hexString.toUpperCase();   
	    int length = hexString.length() / 2;   
	    char[] hexChars = hexString.toCharArray();   
	    byte[] d = new byte[length];   
	    for (int i = 0; i < length; i++) {   
	        int pos = i * 2;   
	        d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));   
	    }   
	    return d;   
	}   
	
	
	public static byte[] intToByte(int val){
		byte[] b = new byte[4];
		b[3] = (byte)(val & 0xff);
		b[2] = (byte)((val >> 8) & 0xff);
		b[1] = (byte)((val >> 16) & 0xff);
		b[0] = (byte)((val >> 24) & 0xff);
		return b;
	}
	
	
	public static int bytesToInt(byte[] bytes, int off) {  
        int b0 = bytes[off] & 0xFF;  
        int b1 = bytes[off + 1] & 0xFF;  
        int b2 = bytes[off + 2] & 0xFF;  
        int b3 = bytes[off + 3] & 0xFF;  
        return (b0 << 24) | (b1 << 16) | (b2 << 8) | b3;  
    }  

	
	
	public static byte enCharToByte(char c) {
//		     byte[] b = new byte[2];
//		     b[0] = (byte) ((c & 0xFF00) >> 8);
		     return (byte) (c & 0xFF);
   }
	
	
	/**
     * 判断一个字符是否是汉字
     * PS：中文汉字的编码范围：[\u4e00-\u9fa5]
     *
     * @param c 需要判断的字符
     * @return 是汉字(true), 不是汉字(false)
     */
    public static boolean isChineseChar(char c) {
    	  Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
          if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                  || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                  || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                  || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                  || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                  || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
              return true;
          }
          return false;
    }
    
    /**
     * 字符串转Unicode
     * @param str
     * @return
     */
    public static String stringToUnicode(char c) {
		StringBuffer sb = new StringBuffer();
			if(Integer.toHexString(c).length()==2) {
				sb.append("00" + Integer.toHexString(c));
			}else if(Integer.toHexString(c).length()==3) {
				sb.append("0" + Integer.toHexString(c));
			}else {
				sb.append(Integer.toHexString(c));
			}
		return sb.toString();
	}
    
    
	/**  
	 * Convert char to byte  
	 * @param c char  
	 * @return byte  
	 */  
	 private static byte charToByte(char c) {   
	    return (byte) "0123456789ABCDEF".indexOf(c);   
	}  
	 
	 public static void main(String[] args) {
		System.out.println(HexUtils.hexStringToBytes("111"));
	}
	 
}
