package tenglang.common.datasource;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "jdbc.config")
public class JdbcConfig {

    private String url;

    private String username;

    private String password;

    private boolean enabled=true;

    private JdbcConfig master;

    private JdbcConfig slave;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public JdbcConfig getMaster() {
        return master;
    }

    public void setMaster(JdbcConfig master) {
        this.master = master;
    }

    public JdbcConfig getSlave() {
        return slave;
    }

    public void setSlave(JdbcConfig slave) {
        this.slave = slave;
    }
}
