package mybatis.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import mybatis.TestVo;

public interface TestVoMapper extends BaseMapper<TestVo> {
}
