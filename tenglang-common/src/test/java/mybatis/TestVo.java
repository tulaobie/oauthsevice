package mybatis;

import com.baomidou.mybatisplus.annotation.TableName;

@TableName("test")
public class TestVo {
    private String data;
    private String name;
    private Short age;
    private Long id;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getAge() {
        return age;
    }

    public void setAge(Short age) {
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
