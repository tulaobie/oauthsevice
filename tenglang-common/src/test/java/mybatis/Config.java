package mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import tenglang.common.datasource.DruidAutoConfiguration;

@TestConfiguration
@Import(DruidAutoConfiguration.class)
@ComponentScan("mybatis")
@MapperScan(basePackages = {"mybatis.base"})
public class Config {
}
