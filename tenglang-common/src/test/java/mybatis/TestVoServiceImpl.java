package mybatis;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import mybatis.base.TestVoMapper;
import org.springframework.stereotype.Service;

@Service
public class TestVoServiceImpl extends ServiceImpl<TestVoMapper, TestVo> implements TestVoService{
}
