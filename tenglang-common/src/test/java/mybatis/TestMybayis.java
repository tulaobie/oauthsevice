package mybatis;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import mybatis.base.TestVoMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import tenglang.common.MybatisTestApplication;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= MybatisTestApplication.class)
@Import(Config.class)
public class TestMybayis {

    @Autowired
    private TestVoService testVoService;

    @Autowired
    private TestVoMapper testVoMapper;

    @Test
    public void testPageHelperFind(){

        PageHelper.startPage(1,10);
        QueryWrapper<TestVo> queryWrapper=new QueryWrapper<>();
        queryWrapper.like("name","%2%");
        List<TestVo> testVoList=testVoService.list(queryWrapper);
        for (TestVo testVo : testVoList) {
            System.out.println(testVo.getName());
        }
        System.out.println(testVoList.getClass().getName());
    }


    @Test
    public void testPlusPageFind(){

        Page<TestVo> page=new Page<>(1,10);
        QueryWrapper<TestVo> queryWrapper=new QueryWrapper<>();
        queryWrapper.like("name","%2%");
        List<TestVo> testVoList=testVoMapper.selectList(queryWrapper);
        for (TestVo testVo : testVoList) {
            System.out.println(testVo.getName());
        }
        System.out.println(testVoList.getClass().getName());
    }



//    @Test
    public void save(){
        for(int a=0;a<100;a++){
            TestVo testVo=new TestVo();
            testVo.setId(a*100L);
            testVo.setName("wangjun"+a);
            testVoService.save(testVo);
        }
    }
}
