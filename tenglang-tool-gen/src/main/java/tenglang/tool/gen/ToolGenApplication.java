package tenglang.tool.gen;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import tenglang.common.datasource.DruidAutoConfiguration;
import tenglang.common.httpclient.HttpClientFeignConfiguration;
import tenglang.common.redis.RedisCache;
import tenglang.tool.gen.config.SwaggerConfig;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@Import({DruidAutoConfiguration.class, SwaggerConfig.class})
@ComponentScan(excludeFilters = {@ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE,value ={HttpClientFeignConfiguration.class, RedisCache.class})})
@MapperScan(basePackages = {"tenglang.tool.gen.mapper"})
public class ToolGenApplication {
    public static void main(String[] args) {
        SpringApplication.run(ToolGenApplication.class, args);
    }
}
