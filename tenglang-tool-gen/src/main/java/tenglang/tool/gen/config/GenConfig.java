package tenglang.tool.gen.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 读取代码生成相关配置
 * 
 * @author ruoyi
 */
@ApiModel
public class GenConfig
{
    /** 作者 */
    @ApiModelProperty(hidden = true)
    public  String author;

    /** 自动去除表前缀，默认是true */
    @ApiModelProperty(hidden = true)
    public  static boolean autoRemovePre;

    /** 表前缀(类名不会包含表前缀) */
    @ApiModelProperty(hidden = true)
    public  static String tablePrefix;

    /** 默认包名 **/
    @ApiModelProperty(hidden = true)
    public static String packageName;

    /**
     * 项目前缀
     */
    @ApiModelProperty(value = "项目标识")
    public  String systemPrefix;

    /**
     * 后端项目目录
     */
    @ApiModelProperty(value = "后端项目路径")
    public  String projectDir;


    @ApiModelProperty(value = "需要导入的表名")
    public String tables;

    /**
     * 前端项目目录
     */
    @ApiModelProperty(value = "前端项目路径")
    private String uiProjectDir;

    @ApiModelProperty(hidden = true)
    public final static String mainSrc="src/main/java/";

    @ApiModelProperty(hidden = true)
    public final static String mainResource="src/main/resources/";

    @ApiModelProperty(hidden = true)
    public final static String mybatisXml="mybatis";

    @ApiModelProperty(hidden = true)
    public final static String controllerName="controller";

    @ApiModelProperty(hidden = true)
    public final static String domainName="domain";

    @ApiModelProperty(hidden = true)
    public final static String mapperName="mapper";

    @ApiModelProperty(hidden = true)
    public final static String serviceName="service";

    @ApiModelProperty(hidden = true)
    public final static String serviceImplName="service/impl";


    @ApiModelProperty(hidden = true)
    public final static String uiMainSrc="src/views/";

    @ApiModelProperty(hidden = true)
    public final static String uiApiSrc="src/api/";

    @ApiModelProperty(hidden = true)
    public final static String uiMockSrc="src/mocks/";


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public String getSystemPrefix() {
        return systemPrefix;
    }

    public void setSystemPrefix(String systemPrefix) {
        this.systemPrefix = systemPrefix;
    }

    public String getProjectDir() {
        return projectDir;
    }


    public void setProjectDir(String projectDir) {
        if(!projectDir.endsWith("\\")){
            this.projectDir = projectDir+"\\";
        }else{
            this.projectDir=projectDir;
        }
    }

    public String getUiProjectDir() {
        return uiProjectDir;
    }

    public void setUiProjectDir(String uiProjectDir) {
        if(!uiProjectDir.endsWith("\\")){
            this.uiProjectDir = uiProjectDir+"\\";
        }else{
            this.uiProjectDir=uiProjectDir;
        }
    }

    public String getTables() {
        return tables;
    }

    public void setTables(String tables) {
        this.tables = tables;
    }
}
