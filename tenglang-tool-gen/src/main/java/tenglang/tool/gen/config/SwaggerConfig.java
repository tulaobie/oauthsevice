package tenglang.tool.gen.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Swagger2的接口配置
 * 
 * @author ruoyi
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig
{
    @Bean
    public Docket demoApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .genericModelSubstitutes(DeferredResult.class)
                .useDefaultResponseMessages(false)
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("tenglang.tool.gen.controller"))
                .paths(PathSelectors.any())//过滤的接口
                .build()
                .apiInfo(demoApiInfo());

    }


    private ApiInfo demoApiInfo() {
        Contact contact = new Contact("wangjun", "", "");
        @SuppressWarnings("rawtypes")
        List<VendorExtension> vendorExtensions = new ArrayList<VendorExtension>();
        ApiInfo apiInfo = new ApiInfo("代码生成接口",//大标题
                "代码生成接口",//小标题
                "0.1",//版本
                "index.html",
                contact,//作者
                "debug",//链接显示文字
                "doc.html",//网站链接
                vendorExtensions
        );
        return apiInfo;
    }

}
