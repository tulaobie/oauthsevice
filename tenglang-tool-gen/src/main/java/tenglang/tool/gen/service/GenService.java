package tenglang.tool.gen.service;

import org.springframework.stereotype.Service;
import tenglang.tool.gen.config.GenConfig;

import java.io.File;

@Service
public class GenService {

    private GenConfig genConfig;

    private  File createjavaModule(){
        String packageName= GenConfig.packageName;
        packageName=packageName.replaceAll("\\.","\\\\");
        File javaModule=new File(genConfig.getProjectDir()+GenConfig.mainSrc+packageName+"\\");
        if(!javaModule.exists()){
            javaModule.mkdirs();
        }
        return javaModule;
    }

    private  File createResourceModule(){
        File resourceModule=new File(genConfig.getProjectDir()+GenConfig.mainResource+"\\");
        if(!resourceModule.exists()){
            resourceModule.mkdirs();
        }
        return resourceModule;
    }

}
