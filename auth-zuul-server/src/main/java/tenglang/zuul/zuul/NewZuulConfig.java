package tenglang.zuul.zuul;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NewZuulConfig {

    @Autowired
    private ZuulProperties zuulProperties;

    @Autowired
    private ServerProperties serverProperties;

    /**
     *
     * 功能描述:
     *
     * @param:
     * @return:
     * @auther: tangshun
     * @date: 2019/4/17 20:18
     */
    @Bean
    public NewZuulRouteLocator routeLocator() {
        NewZuulRouteLocator routeLocator = new NewZuulRouteLocator(
                this.serverProperties.getServlet().getContextPath(), this.zuulProperties);
        return routeLocator;
    }

    @Bean
    public ApplicationListener<ApplicationEvent> zuulRefreshRoutesListener() {
        return new ZuulRefreshListener();
    }
}
