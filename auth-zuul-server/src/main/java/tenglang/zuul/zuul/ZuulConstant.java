package tenglang.zuul.zuul;

public interface ZuulConstant {
    public static final String NACOS_DATA_ID="zuul-server";
    public static final String NACOS_GROUP_ID="zuul_route";
}
