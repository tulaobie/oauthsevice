package tenglang.zuul;

import auth.server.impl.config.AuthServiceConfig;
import org.springframework.boot.actuate.autoconfigure.endpoint.EndpointAutoConfiguration;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import tenglang.common.datasource.DruidAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Import;
import spring.security.jwt.JwtTokenConfig;
import tenglang.common.httpclient.HttpClientFeignConfiguration;
import tenglang.common.redis.RedisCache;
import tenglang.common.redis.RedisConfig;
import tenglang.zuul.config.SecurityConfiguration;
import tenglang.zuul.oauth2.AuthServerConfiguration;
import tenglang.zuul.zuul.NacosServerConfig;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableZuulProxy
@ConditionalOnClass(value = {HealthIndicator.class, EndpointAutoConfiguration.class})
@Import({DruidAutoConfiguration.class,JwtTokenConfig.class, SecurityConfiguration.class,
        HttpClientFeignConfiguration.class, AuthServerConfiguration.class, AuthServiceConfig.class, NacosServerConfig.class, RedisConfig.class})
public class AuthZuulServer {
    public static void main(String[] args) {
        SpringApplication.run(AuthZuulServer.class, args);
    }

    @Bean
    public RedisCache redisCache(){
        return new RedisCache();
    }
}
