package tenglang.zuul.service;

import auth.server.inter.pojo.SysUser;
import auth.server.inter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import tenglang.zuul.pojo.SysUserDetails;

@Service
public class ZuulUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        SysUser sysUser=userService.findUserByUsername(s);
        SysUserDetails sysUserDetails=new SysUserDetails(sysUser);
        return sysUserDetails;
    }
}
