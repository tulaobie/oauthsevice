package tenglang.zuul.oauth2;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import auth.server.impl.service.OauthClientDetailsSerivce;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import tenglang.zuul.service.ZuulOauthClientSerivce;
import tenglang.zuul.service.ZuulUserDetailsService;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfiguration extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private AuthenticationManager authenticationManager;


    @Autowired
    private TokenStore jwtTokenStore;

    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;

    @Autowired
    private OauthClientDetailsSerivce oauthClientDetailsSerivce;

    @Autowired
    private ZuulOauthClientSerivce clientDetailsService;

    @Autowired
    private ZuulUserDetailsService sysUserService;


    @Autowired
    @Qualifier("jwtTokenEnhancer")
    private TokenEnhancer jwtTokenEnhancer;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        //这里client使用存在模式，可以实际过程调整为jdbc的方式
        //这里说明一下，redirectUris的连接可以是多个，这里通过access_token都可以访问的
        //简单点，就是授权的过程
        /*clients.inMemory()
                .withClient("client")
                .secret(passwordEncoder.encode("secret"))
                .authorizedGrantTypes("authorization_code", "refresh_token")
                .scopes("All")
                .autoApprove(true)
                .redirectUris("http://localhost:9001/login", "http://localhost:9002/login", "http://localhost:9003/authorize/login");
        clients.inMemory().withClient("innerClient")
                .secret(passwordEncoder.encode("secret"))
                .authorizedGrantTypes("password","refresh_token")
                .scopes("users").autoApprove(true);*/

//        List<OauthClientDetails> oauthClientDetailsList=oauthClientDetailsSerivce.findAllEnableClients();
//        for (OauthClientDetails oauthClientDetails:oauthClientDetailsList){
//            clients.inMemory().withClient(oauthClientDetails.getClientId())
//                     .autoApprove(oauthClientDetails.getAutoApprove())
//                     .scopes(copyArrayFromString(oauthClientDetails.getScope()))
//                     .authorizedGrantTypes(copyArrayFromString(oauthClientDetails.getAuthorizedGrantTypes()))
//                     .secret(oauthClientDetails.getClientSecret())
//                     .authorities(copyArrayFromString(oauthClientDetails.getAuthorities()))
//                     .redirectUris(copyArrayFromString(oauthClientDetails.getWebServerRedirectUri()));
//        }
        clients.withClientDetails(clientDetailsService);
    }

    private String[] copyArrayFromString(String source){
           if(StringUtils.isBlank(source)){
                return new String[0];
           }
            String s[]=null;
            if(source.indexOf(",")==-1){
                 s=new String[]{source};
            }else{
                 s=source.split(",");
            }
            return s;
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        //权限控制
        security.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .allowFormAuthenticationForClients();

    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        //认证体系使用security的方式
        TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
        List<TokenEnhancer> enhancerList = new ArrayList<>();
        enhancerList.add(jwtTokenEnhancer);
        enhancerList.add(jwtAccessTokenConverter);
        enhancerChain.setTokenEnhancers(enhancerList);

        endpoints.allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST);

        endpoints.authenticationManager(authenticationManager)
                .tokenStore(jwtTokenStore)
                .accessTokenConverter(jwtAccessTokenConverter)
                .userDetailsService(sysUserService)
                .tokenEnhancer(enhancerChain)
                .exceptionTranslator(loggingExceptionTranslator());
//        endpoints.tokenServices(defaultTokenServices());
    }

    @Primary
    @Bean
    public DefaultTokenServices defaultTokenServices(){
        DefaultTokenServices services=new DefaultTokenServices();
        services.setAccessTokenValiditySeconds(3600);//设置20秒过期
        services.setRefreshTokenValiditySeconds(7200);//设置刷新token的过期时间
        services.setSupportRefreshToken(true);
        services.setTokenStore(new JwtTokenStore(jwtAccessTokenConverter));
        return services;
    }

    @Bean
    public WebResponseExceptionTranslator loggingExceptionTranslator() {
        return new DefaultWebResponseExceptionTranslator() {
            private Log log = LogFactory.getLog(getClass());
            @Override
            public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
                //异常堆栈信息输出
                log.error("异常堆栈信息", e);
                return super.translate(e);
            }
        };
    }



    public static void main(String[] args) {
        BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
        System.out.println(passwordEncoder.encode("secret"));
    }
}
