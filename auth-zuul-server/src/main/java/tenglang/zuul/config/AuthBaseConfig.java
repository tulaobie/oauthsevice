package tenglang.zuul.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import tenglang.zuul.service.ZuulOauthClientSerivce;
import tenglang.zuul.service.ZuulUserDetailsService;

@Configurable
public class AuthBaseConfig {

	@Bean
	public ZuulOauthClientSerivce clientDetailsService(){
		return new ZuulOauthClientSerivce();
	}


	@Bean
	public ZuulUserDetailsService sysUserService(){
		return new ZuulUserDetailsService();
	}
}
