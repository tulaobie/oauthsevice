package tenglang.zuul.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import tenglang.zuul.service.ZuulOauthClientSerivce;
import tenglang.zuul.service.ZuulUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableConfigurationProperties({ZuulFilterProperties.class})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    /*@Autowired
    private JwtService jwtService;*/


    @Autowired
    private ZuulOauthClientSerivce clientDetailsService;

    @Autowired
    private ZuulUserDetailsService sysUserService;

    @Autowired
    private ZuulFilterProperties zuulFilterProperties;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .exceptionHandling()
        .and()
            .authorizeRequests()
            .antMatchers(zuulFilterProperties.getPermitAlls().toArray(new String[zuulFilterProperties.getPermitAlls().size()])).permitAll()
            .anyRequest().anonymous()
        .and()
            .formLogin().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        //内存用户不多解释
//        builder.inMemoryAuthentication()
//                .withUser("admin")
//                .password(passwordEncoder.encode("admin"))
//                .roles("ADMIN");
        builder.userDetailsService(sysUserService).passwordEncoder(passwordEncoder());
//        builder.userDetailsService(jwtService).passwordEncoder(passwordEncoder);
    }



    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }



}
