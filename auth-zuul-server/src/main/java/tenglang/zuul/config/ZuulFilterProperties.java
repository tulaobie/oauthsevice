package tenglang.zuul.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@ConfigurationProperties(prefix = "zuul.prop")
@Component
public class ZuulFilterProperties{

    /**
     * 可以匿名访问的资源
     */
    private List<String> anonymous;

    private List<String> permitAlls;

    /**
     * 不记录访问日志
     */
    private List<String> noRecords;

    /**
     * 登录用户均可以访问的资源
     */
    private List<String> auths;

    private String loginUrl;

    private String headerName="Authorization";

    private String requestTokenName="accessToken";


    private String appsDataId="";

    private String appsGroup="";

    private String tokenPrefix="Bearer ";

    private String page403Url;



    public List<String> getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(List<String> anonymous) {
        this.anonymous = anonymous;
    }

    public List<String> getPermitAlls() {
        return permitAlls;
    }

    public void setPermitAlls(List<String> permitAlls) {
        this.permitAlls = permitAlls;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }


    public String getAppsDataId() {
        return appsDataId;
    }

    public void setAppsDataId(String appsDataId) {
        this.appsDataId = appsDataId;
    }

    public String getAppsGroup() {
        return appsGroup;
    }

    public void setAppsGroup(String appsGroup) {
        this.appsGroup = appsGroup;
    }

    public String getTokenPrefix() {
        return tokenPrefix;
    }

    public void setTokenPrefix(String tokenPrefix) {
        this.tokenPrefix = tokenPrefix;
    }

    public String getRequestTokenName() {
        return requestTokenName;
    }

    public void setRequestTokenName(String requestTokenName) {
        this.requestTokenName = requestTokenName;
    }

    public String getPage403Url() {
        return page403Url;
    }

    public void setPage403Url(String page403Url) {
        this.page403Url = page403Url;
    }

    public List<String> getAuths() {
        return auths;
    }

    public void setAuths(List<String> auths) {
        this.auths = auths;
    }

    public List<String> getNoRecords() {
        return noRecords;
    }

    public void setNoRecords(List<String> noRecords) {
        this.noRecords = noRecords;
    }
}
