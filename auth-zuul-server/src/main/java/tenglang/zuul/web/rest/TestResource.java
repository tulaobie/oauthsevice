package tenglang.zuul.web.rest;

import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestResource {

    @GetMapping("/hello")
    @ResponseBody
    public Object test(@RequestParam(name="test") String test) {
        return "hello:"+test;
    }
}
