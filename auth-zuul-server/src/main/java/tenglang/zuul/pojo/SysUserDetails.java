package tenglang.zuul.pojo;

import auth.server.inter.pojo.SysUser;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class SysUserDetails extends SysUser implements UserDetails{

    public SysUserDetails(SysUser user) {
        BeanUtils.copyProperties(user,this);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if(this.getRoleIds()!=null){
            final List<Long> roleKeys= Arrays.asList(this.getRoleIds());
            final Collection<GrantedAuthority> grantedAuthoritys= new ArrayList<GrantedAuthority>();
            if(roleKeys!=null){
                roleKeys.forEach(roleKey ->{
                    GrantedAuthority grantedAuthority=new GrantedAuthority() {
                        @Override
                        public String getAuthority() {
                            return String.valueOf(roleKey);
                        }
                    };
                });
            }
            return grantedAuthoritys;
        }
        return Collections.EMPTY_LIST;
    }


    @Override
    public String getUsername() {
        return this.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return super.getStatus().equals("0");
    }

    @Override
    public boolean isAccountNonLocked() {
        return super.getStatus().equals("0");
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return super.getStatus().equals("0");
    }

    @Override
    public boolean isEnabled() {
        return super.getDelFlag().equals("0");
    }
}
